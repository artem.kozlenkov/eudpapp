package com.hsrw.dpapp;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addListenerOnCountryButton();
        addListenerOnAboutButton();
        addListenerOnDataCommissionerButton();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void openBrowser(View view) {
        String url = (String) view.getTag();
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }

    private void addListenerOnCountryButton() {
        Button btnCountry = findViewById(R.id.menu_button_countries);

        btnCountry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(MainActivity.this, CountryActivity.class);
                startActivity(newIntent);
            }

        });
    }

    private void addListenerOnAboutButton() {
        Button btnAbout = findViewById(R.id.menu_button_about);

        btnAbout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(newIntent);
            }

        });
    }

    private void addListenerOnDataCommissionerButton() {
        Button btnDataCommissioner = findViewById(R.id.menu_button_commissioner);

        btnDataCommissioner.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(MainActivity.this, DataCommissionerActivity.class);
                startActivity(newIntent);
            }

        });
    }
}
