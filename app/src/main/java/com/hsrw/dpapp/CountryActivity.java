package com.hsrw.dpapp;

import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.Spinner;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

public class CountryActivity extends AppCompatActivity {

    private Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        addToolBar();
        addListenerOnButton();
    }

    private void addToolBar() {

        Toolbar CountryToolbar = (Toolbar) findViewById(R.id.toolbar);
        CountryToolbar.setTitle(getString(R.string.menu_button_country));
        CountryToolbar.setNavigationIcon(R.drawable.arrow);
        CountryToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void addListenerOnButton() {
        final Spinner spinner1 = findViewById(R.id.country_spinner);
        Button btnSubmit = findViewById(R.id.menu_button);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            private Class<?> cls = null;

            @Override
            public void onClick(View v) {

                String selection = String.valueOf(spinner1.getSelectedItem());

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

                    String item = (String) spinner1.getAdapter().getItem(0);

                    if (selection.equals(item)) {
                        return;
                    }

                    Intent newIntent = new Intent(getBaseContext(), ShowActivity.class);

                    newIntent.putExtra("selection", selection);
                    startActivity(newIntent);

                    spinner1.setSelection(0);
                }
            }
        });
    }
}
