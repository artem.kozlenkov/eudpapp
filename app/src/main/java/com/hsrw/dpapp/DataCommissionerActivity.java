package com.hsrw.dpapp;

import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

public class DataCommissionerActivity extends AppCompatActivity implements IHelper {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_commissioner);
        addToolBar();
    }

    @Override
    public void addToolBar() {
        Toolbar EuToolbar = (Toolbar) findViewById(R.id.toolbar);
        EuToolbar.setTitle(getString(R.string.menu_button_eu));
        EuToolbar.setNavigationIcon(R.drawable.arrow);
        EuToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
