package com.hsrw.dpapp;

        import androidx.appcompat.app.AppCompatActivity;
        import android.os.Bundle;

public class ShowActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(this.getResourceId());
    }

    private int getResourceId(){
        String selection = getIntent().getStringExtra("selection");
        String fileName = "activity_" + selection.replaceAll(" ", "_").toLowerCase();

        return getResources().getIdentifier(fileName, "layout", getPackageName());
    }
}
